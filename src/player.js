class Player{
    constructor(id, name, score, platform){
        this.id = id;
        this.name = name;
        this.score = score;
        this.platform = platform;
        this.Validate();
    }

    Validate(){
        if (!Number.isInteger(this.id) && this.id > 0){
            throw "A player's ID should be a positive integer"
        }
    }
}