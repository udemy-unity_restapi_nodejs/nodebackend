const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const app = express();

const port = 3000;

// const password = "sander1510";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//DB
mongoose.connect("mongodb+srv://MarlusVinicius:sander1510@myfirstdatabase.smu8e.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
 
const schema = new Schema({
    name:{
        type: String,
        required: [true, "A player should have a name"],
        trim: true,
        unique: true
    },
    level:{
        type: String,
        required: [true, "A player should be in a level"]
    },
    score:{
        type: Number,
        required: [true, "A player should have a score"]
    }
})

const Player = mongoose.model('Player', schema)

app.get("/", function(req, res, next){
    res.status(200).send("<h1>Hello world!</h1>");
})

//Dados do player
app.get("/player", function(req, res, next){
    Player.find({}).then(data => {
        if (data && data.length != 0){
            res.status(200).send(data);
        } else {
            res.status(204).send();
        }
    }).catch(e => {
        res.status(500).send(e);
    }) 
})

app.get("/player/:name", function(req, res, next){
    Player.find({ name: req.params.name}).then(data => {
        if (data && data.length != 0){
            res.status(200).send(data);
        } else {
            res.status(204).send();
        }
    }).catch(e => {
        res.status(500).send(e);
    }) 
})

app.post("/player", function(req, res, next){
    var playerTemp = new Player(req.body);
    playerTemp.save().then(data => {
        if (data && data.length != 0){
            res.status(201).send({
                message: "POST succesfully executed"
            });
        } else {
            res.status(400).send({
                message: "Check the value"
            })
        }
    }).catch(e => {
        res.status(500).send({
            message: "Connection error",
            error: e + " "
        });
    });    
})

app.put("/player/:name", function(req, res, next){
    var query = {name: req.params.name};
    Player.findOneAndUpdate(query, req.body, (error, data) => {
        if (error){
            res.status(500).send(error);
        } else {
            if (data && data.length != 0){
                res.status(202).send({message: "UPDATE succesfully executed"});
            } else {
                res.status(204).send();
            }
        }
    });

})

app.delete("/player/:name", function(req, res, next){
    var query = {name: req.params.name};

    Player.findOneAndDelete(query).then(data => {
        if (data && data.length != 0){
            res.status(202).send({message: "DELETE succesfully executed"});
        }else{
            res.status(204).send()
        }
    }).catch(e => {
        res.status(500).send(e);
    })
})

app.listen(port, function(){
    console.log('Server is listening on port ' + port + '...');
})




